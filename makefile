OBJS = my-router.o 
CXX = g++
DEBUG = -g
CFLAGS =  -Wall -c $(DEBUG) 
LFLAGS = -Wall $(DEBUG)

my-router: $(OBJS)
	$(CXX) $(LFLAGS) $(OBJS) -o my-router

my-router.o: my-router.cpp routing_table_class.cpp 
	$(CXX) $(CFLAGS) my-router.cpp

#routing_table_class.o: routing_table_class.cpp
#	$(CXX) $(CFLAGS) routing_table_class.cpp

#routing_table_class.o : Movie.h Movie.cpp NameList.h Name.h
#  $(CC) $(CFLAGS) routing_table_class.cpp

#DataPacket.o : NameList.h NameList.cpp Name.h 
#  $(CC) $(CFLAGS) NameList.cpp



clean: 
	\rm my-router *.o 
