#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include "routing_table_class.cpp"
#include "DataPacket.cpp"
#include <vector>
#include <string>

//not sure if this is necessary 
#include <iostream>
#include <unistd.h>
#include <time.h>
#include <signal.h>
using namespace std;

#define NODE_A 10000


int create_socket() {
	int fd;

	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("cannot create socket");
		return 0;
	}
	 
	return fd;
}

void exit() {
	exit(0);
}

//run when we receive any packet
// if it returns V, it's a DV 
// if it returns P, it's a data packet 
char getPacketType(string data)
{
	int pos = data.find(',');
	string type = data.substr(0,pos);
	if(type=="dv")
	{
		return 'V';
	}
	else if (type=="data")
	{
		//set to var or something that will prompt packet to be sent
		return 'P';
	}
}

//for data packet: print timestamp, id of source node of packet, final destination router, 
//the udp port in which the packet arrived, and the udp source port along which the packet was forwarded.
void writeToFileData(Routing_table r, string message)
{
	ofstream myfile;

	char src = r.returnsrc();
	string ss = string(1, src);
  	myfile.open ("routing-output"+ ss +".txt", ios::out | ios::app );

	// current date/time based on current system
	time_t now = time(0);
	// convert now to string form
	char* dt = ctime(&now);
	//print timestamp, + routing table (dest, cost, ourport, outgoing port(us), dest, dest port)
	myfile << dt;
	myfile << "\n";

	myfile << "data packet:\n";
	myfile << message;
	myfile << "\n";

	myfile.close();

}


//for dv: timestamp, before table, dv table including neighbor it came from, and the after table
void writeToFileDV(Routing_table r, string tableBefore, string DV)
{
	ofstream myfile;

	char src = r.returnsrc();
	string ss = string(1, src);
  	myfile.open ("routing-output"+ ss +".txt", ios::out | ios::app );

	// current date/time based on current system
	time_t now = time(0);
	// convert now to string form
	char* dt = ctime(&now);
	//print timestamp, + routing table (dest, cost, ourport, outgoing port(us), dest, dest port)
	myfile << dt;
	myfile << "\n";

	//before table
	myfile << "Table Before:\n";
	myfile << tableBefore;

	myfile << "\n";
	myfile << "DV table received:\n";
	myfile << DV;

	myfile << "\n";

	myfile << "Table After:\n";
	for(int i=0; i<r.getTableSize(r.getTable()); i++)
	{
		myfile << r.returnsrc();
		myfile << ",";
		myfile << r.getPort();
		myfile << ",";
		myfile << r.getTable()[i].dest;
		myfile << ",";
		myfile << r.getTable()[i].dest_port;
		myfile << ",";
		myfile << r.getTable()[i].cost;
		myfile << "\n";
	}
	myfile << "\n";
	myfile.close();

}




int main(int argc, char *argv[]) {

	string inputFile = argv[1];
	char nodeName = argv[2][0];
	if(argc < 3)
	{
		cerr<<"Provide input file and node name."<<endl;
		exit(0);
	}

	if(nodeName == 'H') {
		//Create my socket
		int sock_packet = create_socket(); // our socket

		//Create address struct for address I'm sending to
		struct sockaddr_in toAddr; // my address
		socklen_t tolen = sizeof(toAddr);
		memset((char*)&toAddr, 0, sizeof(toAddr)); //clear address struct
		toAddr.sin_family = AF_INET;
		toAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

		//Statically whatever node A is supposed to be.. just for testing data packets
		toAddr.sin_port = htons(NODE_A);

		//create data packet
		DataPacket creation;
		creation.destination_name = 'D';
		creation.source_name = 'A';
		creation.port_of_arrival = NODE_A;
		creation.port_of_forward = NODE_A;
		creation.data_message = "hot pancakes!";

		string packaged = generateDataPacketString(creation);
		const void* pkd = packaged.c_str();

		sendto(sock_packet,pkd,packaged.length(),0,(struct sockaddr *)&toAddr, tolen);

		exit(0);
	}

	if(nodeName == 'K') {
		//Create my socket
		int sock_packet = create_socket(); // our socket

		//Create address struct for address I'm sending to
		struct sockaddr_in toAddr; // my address
		socklen_t tolen = sizeof(toAddr);
		memset((char*)&toAddr, 0, sizeof(toAddr)); //clear address struct
		toAddr.sin_family = AF_INET;
		toAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

		//Statically whatever node A is supposed to be.. just for testing data packets
		toAddr.sin_port = htons(NODE_A);

		string kill ="kill";
		const void* k=kill.c_str();
		sendto(sock_packet,k,4,0,(struct sockaddr *)&toAddr, tolen);

		exit(0);
	}

	////////////////////////
	//CREATE ROUTING TABLE//
	////////////////////////

	Routing_table r = Routing_table(inputFile, nodeName);
	vector<TableEntry> table = r.getTable();

	/////////////////////////////////////
	//CREATE AND SET UP SOCKET AND PORT//
	/////////////////////////////////////


	//Create my socket
	int sock_listen = create_socket(); // our socket

	//Create address struct for myself
	struct sockaddr_in myAddr; // my address
	int srcPort = r.getPort(); //my port number
	socklen_t mylen = sizeof(myAddr);
	memset((char*)&myAddr, 0, sizeof(myAddr)); //clear my address struct
	myAddr.sin_family = AF_INET;
	myAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	myAddr.sin_port = htons(srcPort);

	//Bind my socket to my address struct
	if (bind(sock_listen, (struct sockaddr *)&myAddr, sizeof(myAddr)) < 0) {
		perror("bind failed");
		return 0;
	}
	//cout << "bind worked!" << endl;
	//printf("binded to port %d\n", srcPort);

	//Create address struct for address I'm receiving from
	struct sockaddr_in fromAddr; // remote address
	socklen_t fromlen = sizeof(fromAddr);

	//Create address struct for address I'm sending to
	struct sockaddr_in toAddr; // my address
	socklen_t tolen = sizeof(toAddr);
	memset((char*)&toAddr, 0, sizeof(toAddr)); //clear address struct
	toAddr.sin_family = AF_INET;
	toAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	

	//Create buffer for sending messages
	char sendbuffer[256];

	//Create buffer for receiving messages
	unsigned char rcvrbuffer[2048];
	int recvlen; //number of bytes received

	int n;
	string curDV = "";
	string newDV = "";
	string neighborDV = "";
	time_t start = time(0);
	bool firstTime = true;
	int counter = 0;

	//////////////////////////////////
	//START SENDING DVs TO NEIGHBORS//
	//////////////////////////////////
	while(1)
	{
		pid_t pid;
		curDV = r.createDVstring();

		//cout << "MY Current DV String: \n" << curDV << endl;

		const void* temp = curDV.c_str();

		double seconds_since_start = difftime( time(0), start);


		//send dv string
		if (seconds_since_start > 5 || firstTime)
		{
			start = time(0);
			firstTime = false;
			for (int i = 0; i < r.getTableSize(table); i++)
			{
				if (table[i].isNeighbor)
				{
					toAddr.sin_port = htons(table[i].dest_port);
					sendto(sock_listen,temp,curDV.length(),0,(struct sockaddr *)&toAddr, tolen);

				}
			}
		}

		pid = fork();
		if(pid == 0) {
				signal(SIGINT, exit);
				sleep(4);
				toAddr.sin_port = htons(srcPort);
				char keepAlive[10] = "keepAlive";
				sendto(sock_listen,keepAlive,strlen(keepAlive),0,(struct sockaddr *)&toAddr, tolen);				
				_exit(0);
				kill(pid, SIGKILL);
		} else if(pid > 0) {
			signal(SIGINT, exit);

			//check to make sure all neighbors have been sending to them
			for (int i = 0; i < r.getTableSize(table); i++)
			{
				if (table[i].cost != -1 && table[i].isNeighbor)
				{
					double last = difftime( time(0), table[i].last_time);
					if(last > 10) {
						// //pronounce them dead
						// printf("PRONOUNCED DEAD: %c \n", table[i].dest);
						r.set_value (table[i].dest,-1, r.returnsrc(), r.getPort());						
					}
				}
			}

			//receive messages
			recvlen = recvfrom(sock_listen, rcvrbuffer, 2048, 0, (struct sockaddr *)&fromAddr, &fromlen);
			// printf("received %d bytes\n", recvlen);					
			//if invalid data cannot be read


			if(recvlen < 0) {
				perror("failure to receive data packet\n");
				exit(0);
			}

			rcvrbuffer[recvlen] = '\0';

			//convert buffer file to a string
			string rcvrmessage((char*)rcvrbuffer);

			if(rcvrmessage=="kill")
			{
				break;
			}

			//READ MESSAGE
			char type = getPacketType(rcvrmessage);

			//if we get one from our neighbor, update last_time ! 
			for (int i = 0; i < r.getTableSize(table); i++)
			{
				if (table[i].isNeighbor && table[i].dest_port == 
					ntohs(fromAddr.sin_port)) {
						table[i].last_time = time(0);
				}
			}

			string newDV = curDV;

			//if it's type router
			if(type == 'V') {
				//convert to table

				Routing_table new_table = r.dvStringToTable(rcvrmessage.substr(3));
				
				//save r for output
				string saveCurTable = "";
				for(int i=0; i<r.getTableSize(r.getTable()); i++)
				{
					saveCurTable += r.returnsrc();
					saveCurTable += ",";
					saveCurTable += to_string(r.getPort());
					saveCurTable += ",";
					saveCurTable += r.getTable()[i].dest;
					saveCurTable += ",";
					saveCurTable += to_string(r.getTable()[i].dest_port);
					saveCurTable += ",";
					saveCurTable += to_string(r.getTable()[i].cost);
					saveCurTable += "\n";
				}

				//save dv for output
				string saveDV = "";
				for(int i=0; i<new_table.getTableSize(new_table.getTable()); i++)
				{
					saveDV += new_table.returnsrc();
					saveDV += ",";
					saveDV += to_string(new_table.getPort());
					saveDV += ",";
					saveDV += new_table.getTable()[i].dest;
					saveDV += ",";
					saveDV += to_string(new_table.getTable()[i].dest_port);
					saveDV += ",";
					saveDV += to_string(new_table.getTable()[i].cost);
					saveDV += "\n";
				}

				//run compareDV
				int compare = r.compareDV(new_table);
				newDV = r.createDVstring();

				/*if(curDV != newDV)
				{
					cout << "Changed DV String: \n" << newDV << endl;
				}
				else
				{

				}*/

				//IF THERE WAS AN UPDATE, compare DV should deal and output stuff
				if(compare==1)
				{
					//PRINTING... 
					writeToFileDV(r, saveCurTable, saveDV);
				}		
				

			}

			//if it's type data
			if(type == 'P') {

				//cout << "GOT A DATA PACKET" << endl;
				//FORMAT:
				//type, destination node, source node, port of arrival, port of forward
				DataPacket new_packet;
				string packet_data = rcvrmessage.substr(5);

				int results = evaluateDataPacket(r, packet_data, &new_packet);

				//packet needs to be forwarded somewhere else
				if(results == 1) {

					string packet_string = generateDataPacketString(new_packet);
					const void* pkt_string = packet_string.c_str();

					toAddr.sin_port = htons(new_packet.port_of_forward);
					sendto(sock_listen,pkt_string,packet_string.length(),0,(struct sockaddr *)&toAddr, tolen);
				
					string message;
					message += (new_packet.destination_name);
					message += ",";
					message += (new_packet.source_name);	
					message += ",";
					message += to_string(new_packet.port_of_arrival);
					message += ",";
					message += to_string(new_packet.port_of_forward);	
					
					message += "\n";

					writeToFileData(r, message);


				//packet has been delivered!
				} else if(results == 0) {

					string message;
					message += (new_packet.destination_name);
					message += ",";
					message += (new_packet.source_name);	
					message += ",";
					message += to_string(new_packet.port_of_arrival);
					message += ",";
					message += to_string(new_packet.port_of_forward);	
					
					if(new_packet.destination_name == r.returnsrc())
					{
						message += ",";
						message += new_packet.data_message;	
					}	
					message += "\n";

					writeToFileData(r, message);

				//something bad happened
				} else {
					perror("something went wrong with the data packet");
				}

			}

			memset(rcvrbuffer, '\0', 2048); //clear buffer
			int returnStatus;

			waitpid(pid, &returnStatus, 0);
			if(returnStatus == 0)
				kill(pid, SIGKILL);
		}

	}
	
	return 0;

}


