#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <sstream>
//put all routing table stuff in this file
//created class to hold routing table and have functions

/*For Enyu: added a bunch of functions that should work but honestly might not. 
compareDV contains the main functionality, that one is mostly likely to break.
remember when updating cost, we need to add neighbors distance to target + our distance to neighbor. 

For UDP stuff, full_router is now our main function
*/

using namespace std;

enum calculateResults{updated=1, noChange=0, error=-1};


struct TableEntry {
	char dest;
	int cost;
	int outgoing_port;
	int dest_port;
	bool isNeighbor;
	char sendThrough;
	int sendThroughPort;
	double last_time;
};

class Routing_table{

	vector<TableEntry> table;
	char source_name; 
	int my_port;

	public:
	Routing_table(string inputFile, char nodeName);
	Routing_table(string inputFile);
	char returnsrc();
    void set_value (char dest,int cost, char sendThrough, int sendThroughPort); 
    int getCost(char dest);
    string createDVstring();
    Routing_table dvStringToTable(string dvstring);
    void addRow(char dest, int dest_port, int cost, char sendThrough, int sendThroughPort);
    int compareDV(Routing_table t); 
    int getTableSize(vector<TableEntry> table);
    TableEntry contains(char dvDest);
    void printTable();
    vector<TableEntry> getTable();
    int getPort();
    void routPacket(string data);
};

Routing_table::Routing_table(string inputFile, char nodeName)
{

	source_name = nodeName;

	ifstream infile(inputFile);
	ifstream infile2(inputFile);
	string line;

	while (getline(infile, line))
	{
		
		char destName;
		int destPort;
		
		//find and delete source node from tuple
		int pos = line.find(',');
		line.erase(0,pos+1);

		//find and save dest node from tuple
		pos = line.find(',');
		destName = line.substr(0,pos).at(0);
		line.erase(0,pos+1);

		//find and save port from tuple
		pos = line.find(',');
		destPort = stoi(line.substr(0,pos));

		if (nodeName == destName)
		{
			this->my_port = destPort;
			break;
		}

	}

	while (getline(infile2,line))
	{
		char source;
		char dest;
		int port;
		int cost;

		//find and save source node
		int pos = line.find(',');
		source = line.substr(0,pos).at(0);
		line.erase(0,pos+1);

		if (source == nodeName) //insert tuple to table if source node is me
		{
			//find and save dest node
			pos = line.find(',');
			dest = line.substr(0).at(0);
			line.erase(0,pos+1);

			//find and save port
			pos = line.find(',');
			port = stoi(line.substr(0,pos));
			line.erase(0,pos+1);

			//save cost
			cost = stoi(line);

			TableEntry newEntry;
			newEntry.dest = dest;
			newEntry.cost = cost;
			newEntry.outgoing_port = my_port;
			newEntry.dest_port = port;
			newEntry.isNeighbor = true;
			newEntry.sendThrough = dest;
			newEntry.sendThroughPort = port;
			newEntry.last_time = time(0);
			table.push_back(newEntry);

		}
	}
	//printTable();
}

Routing_table::Routing_table(string inputFile)
{

	source_name = inputFile[0];

	istringstream infile(inputFile);
	istringstream infile2(inputFile);

	string line;

	while (getline(infile, line))
	{
		
		char destName;
		int destPort;
		
		//find and delete source node from tuple
		int pos = line.find(',');
		line.erase(0,pos+1);

		//find and save dest node from tuple
		pos = line.find(',');
		destName = line.substr(0,pos).at(0);
		line.erase(0,pos+1);

		//find and save port from tuple
		pos = line.find(',');
		destPort = stoi(line.substr(0,pos));

		if (source_name == destName)
		{
			this->my_port = destPort;
			break;
		}

	}

	while (getline(infile2,line))
	{
		char source;
		char dest;
		int port;
		int cost;

		//find and save source node
		int pos = line.find(',');
		source = line.substr(0,pos).at(0);
		line.erase(0,pos+1);

		if (source == source_name) //insert tuple to table if source node is me
		{
			//find and save dest node
			pos = line.find(',');
			dest = line.substr(0).at(0);
			line.erase(0,pos+1);

			//find and save port
			pos = line.find(',');
			port = stoi(line.substr(0,pos));
			line.erase(0,pos+1);

			//save cost
			cost = stoi(line);

			TableEntry newEntry;
			newEntry.dest = dest;
			newEntry.cost = cost;
			newEntry.outgoing_port = my_port;
			newEntry.dest_port = port;
			newEntry.isNeighbor = true;
			newEntry.sendThrough = dest;
			newEntry.sendThroughPort = port;
			newEntry.last_time = time(0);
			table.push_back(newEntry);

		}
	}
	//printTable();
}

void Routing_table::printTable() {
	int i = 1;
	printf("Table %c \n", source_name);
	for(TableEntry A : table) {
		printf("TABLE ENTRY %d\n", i);
		printf("destination: %c\n", A.dest);
		printf("cost: %d\n", A.cost);
		printf("outgoing_port: %d\n", A.outgoing_port);
		printf("dest_port: %d\n", A.dest_port);
		printf("is_neighbor: %d\n", A.isNeighbor);
		printf("sendThrough: %c\n", A.sendThrough);	
		printf("sendthroughport: %d\n", A.sendThroughPort);
		printf("\n");	
		i++;
	}
}

//returns our source name (not port number)
char Routing_table::returnsrc()
{
	return source_name;
}

int Routing_table::getTableSize(vector<TableEntry> table)
{
	return table.size();
}


//sets value within table, updates sendThrough also
void Routing_table::set_value(char dest, int cost, char sendThrough, int sendThroughPort)
{
	//printf(dest+"\n");

	for(int i=0; i<table.size(); i++)
	{
		if(table[i].dest == dest)
		{
			table[i].cost = cost;
			table[i].sendThrough = sendThrough;
			table[i].sendThroughPort = sendThroughPort;
			table[i].last_time = time(0);
			return;
		}
	}
	//cerr<<"setting values went wrong"<<endl;
}

//gets cost from dest router
int Routing_table::getCost(char dest)
{
	for(int i=0; i<table.size(); i++)
	{
		if(table[i].dest == dest)
		{
			return table[i].cost;
		}
	}
	return -1;
}

//creates dv message string to send out
string Routing_table::createDVstring()
{
	//go through table, add {dest, cost, outgoing port, dest port}
	string dvString="dv,";
	for(int i=0; i<table.size(); i++)
	{	
		dvString += source_name;
 	 	dvString += ",";
	 	dvString += table[i].dest;
 	 	dvString += ",";
 	 	dvString += to_string(table[i].dest_port);
	 	dvString += ",";
	 	dvString += to_string(table[i].cost);
	 	dvString += "\n";
	}
	return dvString;
}

//takes in dest, cost, outgoing port, dest port
Routing_table Routing_table::dvStringToTable(string dvString)
{
	char node = dvString.at(0);
	Routing_table r(dvString);
	
	return r;
}

//add row to our own routing table
void Routing_table::addRow(char dest, int dest_port, int cost, char sendThrough, int sendThroughPort)
{
	TableEntry newEntry;
	newEntry.dest = dest;
	newEntry.cost = cost;
	newEntry.outgoing_port = my_port;
	newEntry.dest_port = dest_port;
	newEntry.isNeighbor = false;
	newEntry.sendThrough = sendThrough;
	newEntry.sendThroughPort = sendThroughPort;
	newEntry.last_time = time(0);
	table.push_back(newEntry);

}

//returns the tableEntry if both us and the other router have this destination in our table
TableEntry Routing_table::contains(char dvDest)
{
	TableEntry t;
	for(int i=0; i<getTableSize(table); i++)
	{
		if(table[i].dest==dvDest)
		{
			return t=table[i];
		}
	}
	return t;
}

//compare incoming DV to our table and update if necessary
//we examine routing table
//if anything is new, add to our table (create new tableEntry) 
//or update our table 

//return 0 if nothing changed, 1 if there is a change in table
int Routing_table::compareDV(Routing_table other_table)
{
	int changed=0;
	//our cost to the other router
	int cost = getCost(other_table.returnsrc());

	for(int i=0; i<getTableSize(other_table.getTable()); i++)
	{

		//gets the destination from the other table, grabs our entry if we have same entry in our table
		TableEntry n = contains(other_table.getTable()[i].dest);

		//if their dest is us
		if(other_table.getTable()[i].dest == returnsrc())
		{
			//iterate through our table
			for(int j=0; j<getTableSize(table); j++)
			{
				//if our table has their entry
				if(table[j].dest == other_table.returnsrc())
				{
					//if their cost to us is less that our cost to them, update
					if(table[j].cost > other_table.getTable()[i].cost && (other_table.getTable()[i].cost!=-1))
					{
						table[j].cost = other_table.getTable()[i].cost;
						changed=1;
					}
					//our cost to dest is -1
					else if((table[j].cost == -1) && (other_table.getTable()[i].cost>0))
					{
						table[j].cost = other_table.getTable()[i].cost>0;
						changed = 1;
					}
				}
			}
		}
		//we dont have it in our table, its to a port that isnt us
		else if(n.cost==0)
		{	
			// printf("other persons table: \n");
			// other_table.printTable();

			// printf("my table: \n");
			// printTable();

			addRow(other_table.getTable()[i].dest, other_table.getTable()[i].dest_port, other_table.getTable()[i].cost+cost, other_table.returnsrc(), other_table.getPort());
			changed=1;
		}
		//we have this entry in our table already
		else
		{
			//if other table has cost -1, change our table to -1
			if(other_table.getTable()[i].cost==-1)
			{
				set_value(n.dest, -1, other_table.returnsrc(), other_table.getPort());
				changed=1;
			}
			//if our table has -1 and other table has >1, change to >1 cost
			else if((n.cost==-1) && (other_table.getTable()[i].cost>0))
			{
				set_value(n.dest, other_table.getTable()[i].cost+cost, other_table.returnsrc(), other_table.getPort());
				changed=1;
			}
			//if our table has greater cost than other table cost
			else if(n.cost > (other_table.getTable()[i].cost+cost))
			{
				set_value(n.dest, other_table.getTable()[i].cost+cost, other_table.returnsrc(), other_table.getPort());
				changed=1;
			}
			//else, no change is made
			
		}

	}
	return changed;
}

    



vector<TableEntry> Routing_table::getTable()
{
	return table;
}

int Routing_table::getPort()
{
	return my_port;
}


