


struct DataPacket {
		char destination_name;
		char source_name;
		int port_of_arrival;
		int port_of_forward;
		string data_message;
};


//changes data packet back into string
string generateDataPacketString(DataPacket packet)
{
	//write to file the timestamp, the ID(char) of the source node of the data packet, 
	//the destination router(char), the UDP port in which the packet arrived(int), and
	//the UDP source port along which the packet was forwarded(int)

	string message = "data,";
	message += packet.destination_name;
	message += ",";
	message += packet.source_name;	
	message += ",";
	message += to_string(packet.port_of_arrival);
	message += ",";
	message += to_string(packet.port_of_forward);	
	message += ",";
	message += packet.data_message;		
	message += "\n";

	return message;	

}


DataPacket stringToDataPacket(string data) {
	DataPacket packet;

	//erase initial type: "data"
	 string message=data;
	 int pos;

	packet.destination_name = message.at(0);
	pos = message.find(',');
	message.erase(0,pos+1);

	packet.source_name = message.at(0);
	pos = message.find(',');
	message.erase(0,pos+1);

	pos = message.find(',');
	packet.port_of_arrival = stoi(message.substr(0,pos));	
	message.erase(0,pos+1);

	pos = message.find(',');
	packet.port_of_forward = stoi(message.substr(0,pos));	
	message.erase(0,pos+1);

	pos = message.find('\n');
	packet.data_message = message.substr(0,pos);	
	message.erase(0,pos+1);

/*	printf("dest: %c\n", packet.destination_name);
	printf("src: %c\n", packet.source_name);
	printf("port of arrival: %d\n", packet.port_of_arrival);	
	printf("port of forward: %d\n", packet.port_of_forward);
	cout << packet.data_message << endl;*/

	return packet;
}

void timestamp()
{
	time_t ltime;
	ltime=time(NULL);
	printf("%s\n",asctime(localtime(&ltime)));
}

void printDataPacket(DataPacket packet) {
		//ID of the source node
		printf("Source Node ID: %c\n", packet.source_name);
		//destination router
		printf("Destination Node ID: %c\n", packet.destination_name);
		//UDP port which packet arrived
		printf("Arrived through Port: %d\n", packet.port_of_arrival);	
		//UDP port which packet was forwarded
		printf("Forwarded through Port: %d\n", packet.port_of_forward);
}


//changes string back to data packet to send out
//returns 0 if it reaches destination
//returns 1 if it continues
//returns -1 if there was an error

int evaluateDataPacket(Routing_table r, string data, DataPacket * send_packet)
{
	//write to file the timestamp, the ID(char) of the source node of the data packet, 
	//the destination router(char), the UDP port in which the packet arrived(int), and
	//the UDP source port along which the packet was forwarded(int)
	
	//FORMAT OF DATA PACKET
	//type, destination node, source node, port of arrival, port of forward

	//Parsing the string...
	//it will build a DataPacket, which is either printed out or it's forwarded on

	//BUILD DATA PACKET
	DataPacket packet = stringToDataPacket(data);

	//if dest is this router, write ^ all this stuff to the file + the data message
	if(packet.destination_name == r.returnsrc())
	{
		//message printout!!!

		//print out timestamp
		//timestamp();
		//print out packet
		//printDataPacket(packet);

		//Message
		//cout << "Text Phrase:\n" << packet.data_message << endl;	
		*send_packet = packet;	

		return 0;
	}
	//else write stuff to file, and send packet onto next router in path. 
	//will change DataPacket.forwarded --> to say who it's next node is
	else
	{
		//figure out where to send to
		TableEntry entry = r.contains(packet.destination_name);

		//check to make sure it found one
		if(entry.dest == '\0') {
			printf("Couldn't find this destination in my table!");
			exit(0);
		}

		//assign who was the most recent packet to touch it
		packet.port_of_arrival = r.getPort();

		//assign who the packet is going to next!
		TableEntry sendto = r.contains(entry.sendThrough);
		packet.port_of_forward = sendto.dest_port;

		//assign our variable packet to the REAL packet in main()
		*send_packet = packet;

		//print out timestamp
		//timestamp();
		//print out packet
		//printDataPacket(packet);		

		//means we still got things to do.. 
		return 1;
	}

	//should never get here.. but if it is, it didn't work :(
	return -1;
}
